"****************************
" file: plugins.vim
" ***************************

call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'
Plug 'itchyny/lightline.vim'
Plug 'ap/vim-css-color'
Plug 'vimwiki/vimwiki'
Plug 'frazrepo/vim-rainbow'
"plug 'mcchrish/nnn.vim'
"Plug 'jiangmiao/auto-pairs'
call plug#end()

" GRUVBOX
colorscheme gruvbox
set background=dark

" LIGHTLINE
set laststatus=2      "Shows lightline
let g:lightline={
    \ 'colorsheme': 'gruvbox'
    \ }

" VIMWIKI
set nocompatible
let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \'syntax': 'markdown', 'ext': '.md'}]

" RAINBOW
au FileType c,cpp,obj,objcpp call rainbow#load()

"************************
" built in plugins
"************************

" TERMDEBUG
nmap <leader>td1 :packadd termdebug<CR>:Termdebug output<CR><C-K><C-W><S-L><C-H><C-W><S-K><C-J> " option 1
nmap <leader>td2 :packadd termdebug<CR>:Termdebug output<CR><C-W><S-L><C-W><S-H>                " option 2
