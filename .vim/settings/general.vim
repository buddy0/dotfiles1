" ***********************
" file: general.vim
" ************************

syntax on 
filetype plugin on
set nu
set noswapfile
set showcmd   "shows last command in bottom right
let mapleader = " "

" TABS
set expandtab    " replace tabs with spaces
set tabstop=4
set shiftwidth=4

" FOLDING
set foldenable
set foldmethod=manual
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview

" SPLIT NAVIGATION
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow
set splitright

" COMPILE & DEBUG PROGRAMS
nnoremap <F4> :vert term<CR>make<CR>
nnoremap <F5> :vert term<CR>gdb output<CR>
tnoremap <Esc> <C-\><C-n>
tnoremap <C-J> <C-W><C-J>
tnoremap <C-K> <C-W><C-K>
tnoremap <C-L> <C-W><C-L>
tnoremap <C-H> <C-W><C-H>

" DEVELOPMENT
set tags+=/usr/include/tags

